//
//  ViewController.swift
//  AlamofireTesting
//
//  Created by Anton on 05.12.16.
//  Copyright © 2016 Macbook. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource{
    
    var net = Network()
    var response : (String,String,String,String) = ("","","","")
    @IBOutlet weak var pressureResult   : UILabel!
    @IBOutlet weak var tempResult       : UILabel!
    @IBOutlet weak var windSpResult     : UILabel!
    @IBOutlet weak var visibilityResult : UILabel!
    @IBOutlet weak var cityesPicker     : UIPickerView!
    
    var cityesDict = ["London"    :2648110, "Washington":4450315,
                      "Amsterdam" :2759793,"Cecil"      :4350685,
                      "Kiev"      :703448 ,"Moscow"     :524901]
    
    var nameOfCityes = ["London"    ,"Washington",
                        "Amsterdam" ,"Cecil"     ,
                        "Kiev"      ,"Moscow"    ]
    
    let pres        = "Pressure(gPA): "
    let temp        = "Temperature(K): "
    let windSp      = "Wind speed(km/h):"
    let visibility  = "Visibility(meters): "
    

    //MARK: MyMethods
    
    @IBAction func wheatherData(_ sender: UIButton) {
      
        let cityInPicker =  self.cityesPicker.selectedRow(inComponent: 0)
        self.response = net.wheatherInfoByCityID(cityID: cityesDict[nameOfCityes[cityInPicker]]!)
        let temperatureResult = String(Int((Double(self.response.1) ?? 0.0) - 273.15))
        self.pressureResult.text    = pres + self.response.0
        self.tempResult.text        = temp + temperatureResult
        self.windSpResult.text      = windSp + self.response.2
        self.visibilityResult.text  = visibility + self.response.3
    }
    
   
    
    //MARK: PickerDataSourceMethods
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return nameOfCityes.count
    }
    //MARK: PickerDelegateMethods
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return nameOfCityes[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        //print(nameOfCityes[row])
         self.response = net.wheatherInfoByCityID(cityID: cityesDict[nameOfCityes[row]]!)
    }
    
}

